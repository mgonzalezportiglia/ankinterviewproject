const express = require('express')

const PORT = 3001

const app = express()

app.use("/api", (req, res, next) => {
    console.log("NODE EXPRESS SERVER START")
    res.json({message: 'Hello from Node Express server!!'})
})

app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`)
})